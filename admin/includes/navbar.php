<?php include "../resources/db_functions.php" ?>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="http://localhost/admin/member_list.php">Members</a></li>
            <li><a href="http://localhost/admin/post_list.php">Posts</a></li>
            </ul>
          </li>
        </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#">
                    <?php
                      $username = $_SESSION["username"];
                      $user = get_member_with_username($username);
                      echo 'Welcome ' . $user["name"];
                    ?>
                  </a>
                </li>
                <li>
                    <a href="http://localhost/admin/change_password.php">Change Password</a>
                </li>
                <li>
                    <a href="http://localhost/admin/logout.php">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
