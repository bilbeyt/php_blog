<?php
session_start();
if(empty($_SESSION["authenticated"]) || $_SESSION["authenticated"] != 'true') {
    header('Location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/header.php" ?>
        <title>Post List</title>
    </head>
    <body>
        <?php include "includes/navbar.php" ?>
        <div class="container">
            <div class="page-header" style="padding-bottom:40px;">
                <h1><a href="http://localhost/admin/post_create.php" class="pull-right" style="font-size:30px;">Add Post</a>Post List</h1>
            </div>
            <p>See details with clicking # of post</p>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Title</th>
                            <th>Author </th>
                            <th>Summary</th>
                            <th>Created Date</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                        foreach(get_post_list() as $post){
                          $date = new DateTime($post['created_date']);
                          $formattedDate = date_format($date, 'd/m/Y H:i:s');
                          $user = get_member($post["author_id"]);
                          echo '<tr>
                            <td><a href="post_detail.php?id='.$post["id"].'">'.$post["id"].'</a></td>
                            <td>'.$post["title"].'</td>
                            <td>'.$user["name"].' '.$user["surname"].'</td>
                            <td>'.$post["summary"].'</td>
                            <td>'.$formattedDate.'</td>
                          </tr>';
                        }
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php include "includes/footer.php" ?>
    </body>
</html>
