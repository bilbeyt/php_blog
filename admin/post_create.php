<?php
session_start();
if(empty($_SESSION["authenticated"]) || $_SESSION["authenticated"] != 'true') {
    header('Location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/header.php" ?>
        <title>Create Post</title>
    </head>
    <body>
        <?php include "includes/navbar.php" ?>
        <div class="container">
            <div class="page-header" style="padding-bottom:40px;">
                <h1>Create Post</h1>
            </div>
            <form role="form" class="form-normal" method="post" action="">
                <div class="form-group">
                    <label class="control-label " for="authorField">Author</label>
                    <div class="">
                        <select name="author" id="author" class="form-control">
                          <?php
                            foreach( get_member_list() as $member){
                              echo '<option value="'.$member["id"].'">'.$member["name"].' '.$member["surname"].'</option>';
                            }
                          ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label " for="titleField">Title</label>
                    <div class="">
                        <input name="title" type="text" class="form-control" id="titleField" placeholder="Title">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label " for="summaryField">Summary</label>
                    <div class="">
                        <input name="summary" type="text" class="form-control" id="summaryField" placeholder="Summary">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label " for="contentField">Content</label>
                    <div class="">
                        <textarea name="content" class="form-control" rows="3" id="contentField" placeholder="Content"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class=" ">
                        <input name="submit" type="submit" class="btn btn-primary"></input>
                    </div>
                </div>
                <?php
                  function add(){
                    $title = $_POST["title"];
                    $summary = $_POST["summary"];
                    $content = htmlspecialchars($_POST["content"]);
                    $author_id = (int) $_POST["author"];

                    add_post($title, $summary, $content, $author_id);
                    
                    echo '<div class="alert alert-info fade in">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          Adding successful.
                         </div>';
                  }
                  if(isset($_POST["submit"])){
                    add();
                  }
                ?>
            </form>
        </div>
        <?php include "includes/footer.php" ?>
    </body>
</html>
