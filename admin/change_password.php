<?php
session_start();
if(empty($_SESSION["authenticated"]) || $_SESSION["authenticated"] != 'true') {
    header('Location: login.php');
}
$errors = FALSE;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/header.php" ?>
        <title>Change Password</title>
    </head>
    <body>
        <?php include "includes/navbar.php" ?>
        <div class="container">
            <div class="page-header" style="padding-bottom:40px;">
                <h1>Change Password</h1>
            </div>
            <form role="form" class="form-normal" method="post" action="">
                <div class="form-group">
                    <label class="control-label" for="oldPasswordField">Old Password</label>
                    <div>
                        <input name="oldPasswordField" type="password" class="form-control" id="oldPasswordField" placeholder="Old Password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="newPasswordField">New Password</label>
                    <div>
                        <input name="newPassword" type="password" class="form-control" id="newPassword" placeholder="New Password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="confirmPasswordField">Confirm Password</label>
                    <input name="confirmPassword" type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password">
                </div>
                <div class="form-group">
                    <div>
                        <input name="submit" type="submit" class="btn btn-primary"></input>
                    </div>
                </div>
                <?php
                function change(){
                  $username = $_SESSION["username"];
                  $user = get_member_with_username($username);
                  if($_POST["oldPasswordField"] != $user["password"]){
                    echo '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          Passwords mismatched.
                         </div>';
                    $errors = TRUE;
                  }
                  $new_pass = $_POST["newPassword"];
                  $con_password = $_POST["confirmPassword"];
                  if($new_pass != $con_password){
                    echo '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          Confirm password mismatched.
                         </div>';
                    $errors = TRUE;
                  }
                  if(! $errors){
                    change_password($username, $_POST["newPassword"]);
                    echo '<div class="alert alert-info fade in">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          Change successful.
                         </div>';
                  }
                }
                if(isset($_POST["submit"])){
                  change();
                }
                ?>
            </form>
        </div>
        <?php include "includes/footer.php" ?>
    </body>
</html>
