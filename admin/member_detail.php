<?php
session_start();
if(empty($_SESSION["authenticated"]) || $_SESSION["authenticated"] != 'true') {
    header('Location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/header.php" ?>
        <title>Member Detail</title>
    </head>
    <body>
        <?php include "includes/navbar.php" ?>
        <div class="container">
            <div class="page-header" style="padding-bottom:40px;">
                <h1>Member <?php
                  $user = get_member($_GET["id"]);
                  echo $user["name"].' '.$user["surname"];
                ?></h1>
            </div>
            <form role="form" class="form-normal" method="post">
              <?php $user = get_member($_GET["id"]); ?>
                <div class="form-group">
                    <label class="control-label" for="nameField">Name</label>
                    <div>
                        <input name="name" type="text" class="form-control" id="nameField" placeholder="Name" value="<?php echo $user["name"] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="surnameField">Surname</label>
                    <div>
                        <input name="surname" type="text" class="form-control" id="surnameField" placeholder="Surname" value="<?php echo $user["surname"] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="emailField">Email</label>
                    <input name="mail" type="text" class="form-control" id="emailField" placeholder="Email" value="<?php echo $user["mail"] ?>">
                </div>
                <div class="form-group">
                    <label class="control-label" for="usernameField">Username</label>
                    <input name="username" type="text" class="form-control" id="usernameField" placeholder="Username" value="<?php echo $user["username"] ?>">
                </div>
                <div class="form-group">
                    <label class="control-label" for="passwordField">Password</label>
                    <input name="password" type="password" class="form-control" id="passwordField" placeholder="Password" value="<?php echo $user["password"] ?>">
                </div>
                <label>
                  <?php
                    if($user["is_admin"])
                    {
                      echo '<input name="is_admin" class="control-label" type="checkbox" checked="checked")';
                    }
                    else{
                      echo '<input name="is_admin" class="control-label" type="checkbox")';
                    }
                    ?>>Is Admin?
                </label>
                <div class="form-group">
                    <div>
                        <button name="delete" type="submit" class="btn btn-danger">Delete</button>
                        <button name="update" type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                </div>
                <?php
                function delete(){
                  $id = $_GET["id"];
                  delete_member($id);
                  echo '<div class="alert alert-info fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        Deleting successful.
                       </div>';
                }
                if( isset($_POST["delete"])){
                  delete();
                }
                function update(){
                  $id = $_GET["id"];
                  $name = $_POST["name"];
                  $surname = $_POST["surname"];
                  $mail = $_POST["mail"];
                  $username = $_POST["username"];
                  $password = $_POST["password"];
                  $is_admin = (int) isset($_POST["is_admin"]);

                  edit_member($id,$name,$surname,$mail,$username,$password, $is_admin);
                  echo '<div class="alert alert-info fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        Updated successful.
                       </div>';
                }
                if( isset($_POST["update"])){
                  update();
                }
                ?>
            </form>
        </div>
        <?php include "includes/footer.php" ?>
    </body>
</html>
