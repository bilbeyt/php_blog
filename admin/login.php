<?php include "includes/verify_user.php" ?>
<?php
$username = null;
$password = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if(!empty($_POST["username"]) && !empty($_POST["password"])) {
        $username = $_POST["username"];
        $password = $_POST["password"];
        if(verify_user($username, $password)) {

            session_start();
            $_SESSION["authenticated"] = 'true';
            $_SESSION["username"] = $username;
            header('Location: member_list.php');
        }
        else {
            header('Location: login.php');

        }

    } else {
        header('Location: login.php');
    }
} else {
?>
<!DOCTYPE html>
<html lang="en"><head>
    <?php include "includes/header.php" ?>
    <link href="css/signin.css" rel="stylesheet">
    <title>Login</title>
  </head>

  <body>

    <div class="container">

      <form class="form-signin" style="padding-top:300px;" method="POST">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Username</label>
        <input type="text" name="username" id="username" class="form-control" placeholder="Username" required="" autofocus="">
        <label for="password" class="sr-only">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
    <?php include "includes/footer.php" ?>
</body></html>
<?php } ?>
