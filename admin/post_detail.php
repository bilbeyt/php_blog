<?php
session_start();
if(empty($_SESSION["authenticated"]) || $_SESSION["authenticated"] != 'true') {
    header('Location: login.php');

}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/header.php" ?>
        <title>Post Detail</title>
    </head>
    <body>
        <?php include "includes/navbar.php" ?>
        <div class="container">
            <div class="page-header" style="padding-bottom:40px;">
                <h1>Post
                  <?php
                    $post = get_post($_GET["id"]);
                    echo $post["title"];
                  ?>
                  </h1>
            </div>
            <form role="form" class="form-normal" method="post">
                <div class="form-group">
                    <label class="control-label" for="authorField">Author</label>
                    <div>
                        <select name="author" id="authorField" class="form-control">
                          <?php
                            $post = get_post($_GET["id"]);
                            foreach( get_member_list() as $member){

                              if($post["author_id"] == $member["id"]){
                                echo '<option value="'.$member["id"].'" selected>'.$member["name"].' '.$member["surname"].'</option>';
                              }
                              else{
                              echo '<option value="'.$member["id"].'">'.$member["name"].' '.$member["surname"].'</option>';
                              }
                            }
                          ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="titleField">Title</label>
                    <div>
                        <?php
                          echo '<input name="title" type="text" class="form-control" id="titleField" placeholder="Title" value="'.$post["title"].'">'
                        ?>

                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="summaryField">Summary</label>
                    <div>
                        <input name="summary" type="text" class="form-control" id="summaryField" placeholder="Summary" value="<?php echo $post["summary"]; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="contentField">Content</label>
                    <div>
                        <textarea name="content" class="form-control" rows="3" id="contentField" placeholder="Content"><?php echo htmlspecialchars($post["content"]); ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class=" ">
                        <button name="delete" type="submit" class="btn btn-danger">Delete</button>
                        <button name="update" type="submit" class="btn pull-right btn-primary">Update</button>
                    </div>
                </div>
                <?php
                  function delete(){

                    $id = $_GET["id"];
                    delete_post($id);
                    echo '<div class="alert alert-info fade in">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          Deleting successful.
                         </div>';
                  }
                  if( isset($_POST["delete"])){
                    delete();
                  }
                  function update(){
                    $id = $_GET["id"];
                    $title = $_POST["title"];
                    $summary = $_POST["summary"];
                    $content = htmlspecialchars($_POST["content"]);
                    $author_id = (int) $_POST["author"];

                    edit_post($id,$title,$summary,$content,$author_id);
                    echo '<div class="alert alert-info fade in">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          Updated successful.
                         </div>';
                  }
                  if( isset($_POST["update"])){
                    update();
                  }
                ?>
            </form>
        </div>
        <?php include "includes/footer.php" ?>
    </body>
</html>
