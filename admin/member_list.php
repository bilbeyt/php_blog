<?php
session_start();
if(empty($_SESSION["authenticated"]) || $_SESSION["authenticated"] != 'true') {
    header('Location: login.php');
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/header.php" ?>
        <title>Member List</title>
    </head>
    <body>
        <?php include "includes/navbar.php" ?>
        <div class="container">
            <div class="page-header" style="padding-bottom:40px;">
                <h1><a href="http://localhost/admin/member_create.php" class="pull-right" style="font-size:30px;">Add Member</a>Member List</h1>
            </div>
            <p>See details with clicking # of member</p>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                          foreach(get_member_list() as $member){
                            echo '<tr>
                              <td><a href="member_detail.php?id='.$member["id"].'">'.$member["id"].'</a></td>
                              <td>'.$member["name"].'</td>
                              <td>'.$member["surname"].'</td>
                              <td>'.$member["username"].'</td>
                            </tr>';
                          }

                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php include "includes/footer.php" ?>
    </body>
</html>
