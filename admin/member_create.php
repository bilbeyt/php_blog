<?php
session_start();
if(empty($_SESSION["authenticated"]) || $_SESSION["authenticated"] != 'true') {
    header('Location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/header.php" ?>
        <title>Create Member</title>
    </head>
    <body>
        <?php include "includes/navbar.php" ?>
        <div class="container">
            <div class="page-header" style="padding-bottom:40px;">
                <h1>Create Member</h1>
            </div>
            <form role="form" class="form-normal" method="post">
                <div class="form-group">
                    <label class="control-label" for="nameField">Name</label>
                    <div>
                        <input name="name" type="text" class="form-control" id="nameField" placeholder="Name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="surnameField">Surname</label>
                    <div>
                        <input name="surname" type="text" class="form-control" id="surnameField" placeholder="Surname">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="emailField">Email</label>
                    <input name="mail" type="text" class="form-control" id="emailField" placeholder="Email">
                </div>
                <div class="form-group">
                    <label class="control-label" for="usernameField">Username</label>
                    <input name="username" type="text" class="form-control" id="usernameField" placeholder="Username">
                </div>
                <div class="form-group">
                    <label class="control-label" for="passwordField">Password</label>
                    <input name="password" type="password" class="form-control" id="passwordField" placeholder="Password">
                </div>
                <label>
                    <input name="is_admin" class="control-label" type="checkbox" value="">Is Admin?
                </label>
                <div class="form-group">
                    <div>
                        <input name="submit" type="submit" class="btn btn-primary"></input>
                    </div>
                </div>
            </form>
            <?php
            function add(){
              $name = $_POST["name"];
              $surname = $_POST["surname"];
              $mail = $_POST["mail"];
              $username = $_POST["username"];
              $password = $_POST["password"];
              $is_admin = isset($_POST["is_admin"]);

              add_member($name, $surname, $mail, $username, $password, $is_admin);
              echo '<div class="alert alert-info fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Adding successful.
                   </div>';
             }
             if(isset($_POST["submit"])){
               add();
             }
            ?>
        </div>
        <?php include "includes/footer.php" ?>
    </body>
</html>
