<?php
session_start();
if(empty($_SESSION["authenticated"]) || $_SESSION["authenticated"] != 'true') {
    header('Location: login.php');
}
session_destroy(); //destroy the session
header("location:/admin/index.php"); //to redirect back to "index.php" after logging out
exit();
?>
