<?php

include "initial.php";

function add_member($name, $surname, $mail, $username, $password, $is_admin){
  $name = mysql_real_escape_string($name);
  $surname = mysql_real_escape_string($surname);
  $username = mysql_real_escape_string($username);
  $password = mysql_real_escape_string($password);
  $mail = mysql_real_escape_string($mail);

  mysql_query("INSERT INTO members VALUES(null,'{$name}','{$surname}','{$mail}','{$username}','{$password}','{$is_admin}',null)");

}

function edit_member($id, $name, $surname, $mail, $username, $password, $is_admin){
  $id = (int) $id;
  $name = mysql_real_escape_string($name);
  $surname = mysql_real_escape_string($surname);
  $username = mysql_real_escape_string($username);
  $password = mysql_real_escape_string($password);
  $mail = mysql_real_escape_string($mail);

  mysql_query("UPDATE members SET
    name = '{$name}',
    surname = '{$surname}',
    mail = '{$mail}',
    username = '{$username}',
    password = '{$password}'
    WHERE id = {$id}");
}

function delete_member($id){
  $id = (int) $id;

  mysql_query("DELETE FROM members WHERE id = '{$id}'");
}

function delete_post($id){
  $id = (int) $id;

  mysql_query("DELETE FROM posts WHERE id = {$id}");
}

function add_post($title, $summary, $content, $author_id){
  $title = mysql_real_escape_string($title);
  $summary = mysql_real_escape_string($summary);
  $content = mysql_real_escape_string($content);
  $author_id = (int) $author_id;

  mysql_query("INSERT INTO posts VALUES(null,'{$author_id}','{$title}','{$summary}','{$content}',null)");
}

function edit_post($id, $title, $summary, $content, $author_id){
  $id = (int) $id;
  $title = mysql_real_escape_string($title);
  $summary = mysql_real_escape_string($summary);
  $content = mysql_real_escape_string($content);
  $author_id = (int) $author_id;

  mysql_query("UPDATE posts SET
    title = '{$title}',
    summary = '{$summary}',
    content = '{$content}',
    author_id = {$author_id}
    WHERE id = {$id}");
}

function get_post_list($author_id = null){
  $post_list = array();
  $query = "SELECT * FROM posts ";


  if(isset($author_id)){
    $author_id = (int) $author_id;
    $query .= "WHERE 'author_id' = {$author_id}";
  }

  $query .= " ORDER BY posts . created_date DESC";
  $result = mysql_query($query);

  while($row = mysql_fetch_assoc($result)){
    $post_list[] = $row;
  }

  return $post_list;
}

function get_member_list(){
  $member_list = array();
  $query = "SELECT * FROM members ";
  $result = mysql_query($query);
  while($row = mysql_fetch_assoc($result)){
    $member_list[] = $row;
  }

  return $member_list;
}

function get_post($id){
  $id = (int) $id;
  $query = "SELECT * FROM posts WHERE id={$id}";
  $result = mysql_query($query);
  $post = mysql_fetch_assoc($result);
  return $post;
}

function get_member($id){
  $id = (int) $id;
  $query = "SELECT * FROM members WHERE id={$id}";
  $member = mysql_fetch_assoc(mysql_query($query));
  return $member;
}

function get_member_with_username($username){
  $username = mysql_real_escape_string($username);
  $query = "SELECT * FROM members WHERE username='{$username}'";
  $result = mysql_query($query);
  $member = mysql_fetch_assoc($result);
  return $member;
}

function change_password($username, $password){
  $username = mysql_real_escape_string($username);
  $password = mysql_real_escape_string($password);
  $query = "UPDATE members SET password='{$password}' WHERE username='{$username}'";
  $result = mysql_query($query);
}
?>
