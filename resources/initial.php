<?php

include_once('config.php');

$conn = mysql_connect('localhost', 'blog_db_admin', '123321123');
$conn_db = mysql_select_db('blog');

if(! $conn){
  die("Could not connected: " . mysql_error());
}

$sql = "CREATE TABLE IF NOT EXISTS members( ".
    "id INT NOT NULL AUTO_INCREMENT, ".
    "name VARCHAR(70) NOT NULL, ".
    "surname VARCHAR(70) NOT NULL, ".
    "mail VARCHAR(70) NOT NULL, ".
    "username VARCHAR(70) NOT NULL," .
    "password VARCHAR(70) NOT NULL, ".
    "is_admin TINYINT(1), ".
    "created_date TIMESTAMP, ".
    "PRIMARY KEY(id), ".
    "UNIQUE(username));";

 $errors = mysql_query($sql, $conn);
 if(! $errors){
   die("Could not create table members: " . mysql_error());
 }

$sql = "CREATE TABLE IF NOT EXISTS posts( ".
    "id INT NOT NULL AUTO_INCREMENT, ".
    "author_id INT NOT NULL, ".
    "title VARCHAR(70) NOT NULL, ".
    "summary VARCHAR(70) NOT NULL, ".
    "content TEXT, ".
    "created_date TIMESTAMP, ".
    "PRIMARY KEY(id), ".
    "FOREIGN KEY(author_id) REFERENCES members(id) ".
    "ON DELETE CASCADE);";

 $errors = mysql_query($sql, $conn);
 if(! $errors){
   die("Could not create table posts: " . mysql_error());
 }



?>
